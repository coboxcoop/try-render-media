const yo = require('yo-yo')
const render = require('render-media')
const http = require('stream-http')
const { Transform } = require('readable-stream')

const resource = 'localhost'
const port = 1234

function buildOptions (file) {
  return {
    hostname: resource,
    port: port,
    path: '/file/' + file.fileToGet + '/' + file.driveAddress + '/' + file.encryptionKey + '/' + file.start + '/' + file.end,
    method: 'GET'
  }
}

function createStreamFile (file) {
  return {
    name: file.fileToGet,
    createReadStream: function (opts = {}) {
      file.start = opts.start || 0
      file.end = opts.end
      const transform = new Transform({
        transform (chunk, enc, callback) {
          this.push(chunk)
          callback()
        }
      })
      const req = http.request(buildOptions(file), (res) => {
        console.log(`STATUS: ${res.statusCode}`)
        res.pipe(transform)
      })
      req.on('error', (e) => {
        console.error(`problem with request: ${e.message}`)
      })
      req.end()
      return transform
    }
  }
}

const files = [
  {
    fileToGet: 'coraving.jpeg',
    encryptionKey: '6339161585e4d2ed6fb1baa46ca7da79b8b5e82176ea865cee15f7601f9a327c',
    driveAddress: 'e174b1baeb3f2867be061d2fcec33a62e060bd0a52874acd816813f4f56c2bb1'
  },
  {
    fileToGet: 'How-we-all-feel-right-now.mp4',
    encryptionKey: '157532dbe1c14b228cf045d9439693e7b1b5ae1a22edbcf8b88fb4b2d4962b40',
    driveAddress: 'e174b1baeb3f2867be061d2fcec33a62e060bd0a52874acd816813f4f56c2bb1'
  },
  {
    fileToGet: 'big.mp4',
    encryptionKey: '0afdea46d38712baf978a85aece76ad2ae1b8380de2ef9743ead0258d197194a',
    driveAddress: '886952ee5917760f989ac73332e901ab11ca9544315f091758af0c7788b44729'
  },
  {
    fileToGet: 'CelticFairyTales-JosephJacobs.pdf',
    encryptionKey: 'e4dc7af511ae69c29e91258637793cd852a5d577dbb19fd4cfaafeec41165cce',
    driveAddress: 'e174b1baeb3f2867be061d2fcec33a62e060bd0a52874acd816813f4f56c2bb1'
  },
  {
    fileToGet: '01-Soom-T-DirtyMoney.mp3',
    encryptionKey: '9eb8dbba6f9dc3435b1385e2a8d10ad4372e0b1ca1641647b04f785bfdd0b76f',
    driveAddress: 'e174b1baeb3f2867be061d2fcec33a62e060bd0a52874acd816813f4f56c2bb1'
  }
]

const fileButtons = yo`<div>
${files.map(function (item) {
  return yo`<button onclick=${onClick(item)}>${item.fileToGet}</button>`
})}
  </div>`

function onClick (file) {
  return function () {
    render.append(createStreamFile(file), 'body', function (err, elem) {
      if (err) return console.error(err.message)
      console.log(elem)
    })
  }
}

document.body.appendChild(fileButtons)
